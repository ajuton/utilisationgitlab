# Assurer le suivi de version Git d'un projet STM32CubeIDE avec un dépôt GitLab

## Intégrer les outils Git de suivi de version à STM32CubeIDE
STM32CubeIDE est un environnement de développement basé sur Eclipse. Les outils de suivi de version d'Eclipse sont donc disponibles et permettent de gérer les versions sans passer par la ligne de commande.
Ajouter le plugin Egit (Git Integration for Eclipse) :

* _Help > Eclipse Marketplace_
* _Find_ Egit

Il est alors possible d'accéder à quelques outils Git via _Window > Show View > Others_

## Associer un dépôt local Git pour le suivi de version d'un projet STM32CubeIDE

Le suivi local de version d'un projet permet d'avoir accès aux différentes évolutions du projet et de revenir en arrière si besoin.
Pour créer un dépôt Git local à un projet, cliquer droit sur le nom du projet puis _Team > Share Project..._ 

<img src="https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/fenetre_ShareProject.png" width="640"/>

Dans la fenêtre _Share Project_ cliquer sur _Create..._ et indiquer un chemin pour votre dépôt Git.

![fenetreCreateGitRep](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/fenetreCreateGitRep.png)

Accepter les changements.

Pour indiquer les fichiers à ajouter dans le dépôt, cliquer droit sur le dossier du projet et sélectionner _Team > Add To Index..._ Il est possible de ne pas inclure le dossier _Debug_ en cliquant droit dessus puis _Team > Ignore_
Dans la vue _Git Staging_, inscrire un texte pour ce premier point "Commit" et cliquer sur Commit.

A chaque modification, le fichier modifié est indiqué sous l'onglet _Unstaged Changes_. Quand les changements méritent un commit, ajouter les fichiers intéressants modifiés (le main.c, le fichier ioc...) à _Staged Changes_ via la croix verte (_Add selected files to the Index_). Insrcire un message et cliquer sur _Commit_

![Vue_GitStaging](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/Vue_GitStaging.png)

Il est possible de voir les différentes évolutions du projet (clic droit sur le projet puis _Team > Show in History_) et de repartir d'une de ces évolutions, de créer une branche, etc...

![Vue_History](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/Vue_History.png)

## Association du dépôt Git local à un dépôt Gitlab

Créer un projet GitLab pour accueillir votre projet STM32CubeIDE, avec un README.txt et une branche master
Copier le lien HTTPS (bouton Clone)
Dans le dossier dépôt du projet STM32CubeIDE, où se trouve le fichier (caché) .git, faire venir le contenu du dépôt GitLab nouvellement créé. l'ajout `--allow-unrelated-histories` autorise l'association des 2 dépôts n'ayant aucun lien entre eux jusqu'à présent :

`$ git pull --allow-unrelated-histories https://gitlab.com/ajuton/stm32cubeide_gitlab.git master`

Dans STM32CubeIDE, il est alors possible de pousser le dépôt vers GitLab lorsqu'on fait un commit dans la vue _Git Staging_ Il suffit d'indiquer l'URL du dépôt  (https://...), le login et le mot de passe. Il est possible d'enregistrer login et mot de passe de façon sécurisée dans STM32CubeIDE.

![fenetre_PushBranchMaster](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/fenetre_PushBranchMaster.png)


