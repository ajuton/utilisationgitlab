# Générer un site web à l'aide de MkDocs

MkDocs est un outil de génération automatique de site web statique (Static Site Generator SSG) à partir de documents au format Markdown. Il est souvent associé à des dépôts Git. La documentation complète est disponible sur le site www.mkdocs.org

D'autres outils équivalents existent : Jekyll, Nexo, Middleman...

Le site généré est statique, c'est-à-dire que le contenu des pages ne change pas en fonction de ce que fait l'utilisateur. Celui-ci peut juste naviguer entre les pages générées à partir de vos documents.

MkDocs propose plusieurs thèmes, natifs ou via des extensions. ReadTheDocs est le thème utilisé pour cette présentation. Les autres sont présentés sur le site www.mkdocs.org 

L'exemple utilise GitLab. La documentation pour l'intégration de MkDocs dans GitLab est disponile ici : https://about.gitlab.com/stages-devops-lifecycle/pages/

## Installation de MkDocs

Pour installer MkDocs sur sa machine, il est possible d'utiliser un gestionnaire de paquets (apt sous Linux) ou pip.
* `apt install mkdocs`
* `pip install mkdocs` ou `python -m pip install mkdocs`

## Création des fichiers de configuration

Dans le dossier où se trouve le dépôt Git du projet à suivre, utiliser la commande `mkdocs new nom_du_depot` pour créer le dossier docs et le fichier de configuration mkdocs.yml

* `~/git$ mkdocs new depot_STM32Git/`

  INFO    -  Writing config file: depot_STM32Git/mkdocs.yml 
  INFO    -  Writing initial docs: depot_STM32Git/docs/index.md 

* `~$ cd git/depot_STM32Git/`
* `~/git/depot_STM32Git$ ls`

  demo_STM32CubeIDE_GitLab  docs  mkdocs.yml readme.md

![copie ecran arborescence](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/arborescence.png)

mkdocs.yml contient le nom du site. Indiquer un nom significatif, le premier fichier à afficher et le thème choisi (ici readthedocs).
Ajouter l'url du site web associé (https://ajuton.gitlab.io/)

![copie ecran mkdocs yml](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/mkdocs_yml.png)

Le dossier docs contient un premier fichier Markdown à modifier : index.md. Compléter le fichier avec une description très sommaire du projet pour l'instant.

![copie ecran index.md](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/index_md.png)

Il est habituel d'ajouter un dossier __images__ dans le dossier __docs__ pour stocker les images de la documentation.

## Construction et mise en ligne du site

Une fois les pages correspondant à l'attendu, on génère les fichiers html du site via la commande : 

* `~/git/depot_STM32Git$ mkdocs build`

  INFO -  Cleaning site directory 
  INFO -  Building documentation to directory: /home/ajuton/git/depot_STM32Git/site 
  INFO -  Documentation built in 0.04 seconds 

La commande a généré un dossier site

* `~/git/depot_STM32Git$ ls`

  demo_STM32CubeIDE_GitLab  docs  mkdocs.yml  README.md  site

Un dossier __site__ est apparu, que l'on peut tester localement si besoin (commande `mkdocs serve` et site web en 127.0.0.1:8000)

Il suffit alors d'uploader l'ensemble (fichier.yml et dossiers docs mais pas le dossier site, qui sera généré via Gitlab).
* `~/git/depot_STM32Git$ git add docs site mkdocs.yml`
* `~/git/depot_STM32Git$ git commit -a`
* `~/git/depot_STM32Git$ git push origin master`

Activer si ce n'est déjà fait dans GitLab les free Shared Runner (dans *Settings > CI/CD*). Les free Shared Runner sont des programmes s'exécutant dans GitLab.

![copie ecran EnableFreeSharedRunner](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/ValidationFreeShareRunner.png)

Pour déclencher la création du site, il faut un petit script au format yml. Pour cela, créer une pipeline (une exécution d'un script) dans *CI/CD > Pipelines > Editor > Create New CI/CD Pipeline*.

![Copie écran NewPipeline](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/CreatePipeline.png)

Un fichier .gitlab_ci.yml est créé. Cliquer sur Commit pour valider sa création.

![Copie écran Editeur gitlab_ci_yml vide](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/Editeur_gitalb_ci_yml.png)

Dans le dépôt (Repository), coller alors le script suivant dans .gitlab-ci.yml :
```
image: python:latest
pages:
  stage: deploy
  only:
    - master
  script:
    - pip install mkdocs-material
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
```

![Copie écran Editeur gitlab_ci_yml](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/gitlab_ci_yml.png)

Valider le changement (Commit). Cette validation relance le pipeline. Vérifier que le pipeline s'exécute correctement. Sinon, un clic sur le statut *Failed* donne des indications sur l'erreur apparue.

![Suiv statut Pipeline](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/Statut_Pipelines.png)

## Consultation et enrichissement du site web

30 mn plus tard, le site web est alors disponible à l'adresse : https://nom_utilisateur.gitlab.io/nom_depot/
Cette adresse est indiquée dans *Settings > Page*

![Copie Ecran Settings > Page](https://gitlab.com/ajuton/utilisationgitlab/-/raw/master/Images/settings_pages.png)

L'exemple ayant servi pour cette présentation est ainsi : https://ajuton.gitlab.io/stm32cubeide_gitlab/

A chaque changement envoyé sur le dépôt, la génération du site s'exécute de nouveau (activité visible dans *CI/CD > Pipeline*)
Attention, comme des changements ont été effectué sur le dépôt (l'écriture de .gitlab_io.yml), il est probablement nécessaire de récupérer les modifications avant d'en envoyer de nouvelles :

* `git pull origin master`

Il est alors possible de modifier index.md ou d'ajouter des pages .md, en ajoutant leur nom sous *Home : index.md* dans mkdocs.yml.
